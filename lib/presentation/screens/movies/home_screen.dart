import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:peliculas/presentation/bloc/movies/movies_bloc.dart';
import 'package:peliculas/presentation/bloc/movies/now_playing/now_playing_bloc.dart';
import 'package:peliculas/presentation/bloc/movies/popular/popular_bloc.dart';
import 'package:peliculas/presentation/bloc/movies/top_rated/top_rated_bloc.dart';
import 'package:peliculas/presentation/bloc/movies/upcoming/upcoming_bloc.dart';

import '../../widgets/widgets.dart';

class HomeScreen extends StatelessWidget {
  static const name = 'home_screen';

  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          titleSpacing: 0,
          leading: const Icon(Icons.movie_outlined),
          title:
              Text('MoviesApp', style: Theme.of(context).textTheme.titleMedium),
          elevation: 0,
          actions: const [
            IconButton(icon: Icon(Icons.search), onPressed: null)
          ],
        ),
        body: const SingleChildScrollView(
            child: Column(
                children: [NowPlaying(), TopRated(), Popular(), Upcoming()])),
        bottomNavigationBar: const BottomNavigation());
  }
}

class TopRated extends StatelessWidget {
  const TopRated({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocProvider<TopRatedBloc>(
      create: (context) => TopRatedBloc()..add(const FetchMovies()),
      child: BlocBuilder<TopRatedBloc, MoviesState>(
        builder: (context, state) {
          if (state is MoviesLoading) {
            return const LoadingWidget(message: 'Loading');
          } else if (state is MoviesLoaded) {
            return MoviesSlider(
              movies: state.movies,
              title: 'Top rated',
              subTitle: 'di tutti i tempi',
              loadNextPage: () => {
                // Trigger FetchMoreMovies event
                BlocProvider.of<TopRatedBloc>(context).add(FetchMoreMovies(
                    BlocProvider.of<TopRatedBloc>(context).state.props.length))
              },
            );
          } else if (state is MoviesError) {
            return WarningMessage(message: state.error);
          } else {
            return const LoadingWidget(message: 'ERROR');
          }
        },
      ),
    );
  }
}

class Upcoming extends StatelessWidget {
  const Upcoming({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<UpcomingBloc>(
      create: (context) => UpcomingBloc()..add(const FetchMovies()),
      child: BlocBuilder<UpcomingBloc, MoviesState>(
        builder: (context, state) {
          if (state is MoviesLoading) {
            return const LoadingWidget(message: 'Loading');
          } else if (state is MoviesLoaded) {
            return MoviesSlider(
              movies: state.movies,
              title: 'Prossimamente',
              subTitle: 'Aprile 2024',
              loadNextPage: () => {
                // Trigger FetchMoreMovies event
                BlocProvider.of<UpcomingBloc>(context).add(FetchMoreMovies(
                    BlocProvider.of<UpcomingBloc>(context).state.props.length))
              },
            );
          } else if (state is MoviesError) {
            return WarningMessage(message: state.error);
          } else {
            return const LoadingWidget(message: 'ERROR');
          }
        },
      ),
    );
  }
}

class Popular extends StatelessWidget {
  const Popular({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PopularBloc>(
      create: (context) => PopularBloc()..add(const FetchMovies()),
      child: BlocBuilder<PopularBloc, MoviesState>(
        builder: (context, state) {
          if (state is MoviesLoading) {
            return const LoadingWidget(message: 'Loading');
          } else if (state is MoviesLoaded) {
            return MoviesSlider(
              movies: state.movies,
              title: 'Populari',
              loadNextPage: () => {
                // Trigger FetchMoreMovies event
                BlocProvider.of<PopularBloc>(context).add(FetchMoreMovies(
                    BlocProvider.of<PopularBloc>(context).state.props.length))
              },
            );
          } else if (state is MoviesError) {
            return WarningMessage(message: state.error);
          } else {
            return const LoadingWidget(message: 'ERROR');
          }
        },
      ),
    );
  }
}

class NowPlaying extends StatelessWidget {
  const NowPlaying({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<NowPlayingBloc>(
      create: (context) => NowPlayingBloc()..add(const FetchMovies()),
      child: BlocBuilder<NowPlayingBloc, MoviesState>(
        builder: (context, state) {
          if (state is MoviesLoading) {
            return const LoadingWidget(message: 'Loading');
          } else if (state is MoviesLoaded) {
            return CardSwiper(movies: state.movies);
          } else if (state is MoviesError) {
            return WarningMessage(message: state.error);
          } else {
            return const LoadingWidget(message: 'ERROR');
          }
        },
      ),
    );
  }
}
