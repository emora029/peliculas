import 'package:flutter/material.dart';

class WarningMessage extends StatelessWidget {
  final String message;
  const WarningMessage({super.key, required this.message});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const SizedBox(width: 130, height: 130),
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Text(message, textAlign: TextAlign.center, maxLines: 8),
        ),
      ],
    );
  }
}
