import 'package:flutter/material.dart';

class BottomNavigation extends StatelessWidget {
  const BottomNavigation({super.key});

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(elevation: 0, items: const [
      BottomNavigationBarItem(icon: Icon(Icons.home_max), label: 'Inizio'),
      BottomNavigationBarItem(
          icon: Icon(Icons.label_outline), label: 'Categorie'),
      BottomNavigationBarItem(
          icon: Icon(Icons.favorite_outline), label: 'Preferiti'),
    ]);
  }
}
