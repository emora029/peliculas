import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:peliculas/domain/entities/movie.dart';

part 'movies_event.dart';
part 'movies_state.dart';

class MoviesBloc extends Bloc<MoviesEvent, MoviesState> {
  int _currentPage = 0;
  bool _isLoading = false;

  MoviesBloc() : super(MoviesInitial()) {
    on<FetchMovies>((event, emit) async {
      emit(MoviesLoading());
      try {
        final movies = await onMoviesFetched();
        emit(MoviesLoaded(movies));
      } catch (error) {
        debugPrint(error.toString());
        emit(MoviesError(error.toString()));
      }
    });

    on<FetchMoreMovies>((event, emit) async {
      if (_isLoading) return;
      _isLoading = true;
      if (state is MoviesLoaded) {
        debugPrint('isLoading');
        final currentState = state as MoviesLoaded;
        _currentPage++;
        try {
          final newMovies = await onMoviesMoreFetched(_currentPage);
          final allMovies = currentState.movies + newMovies;
          emit(MoviesLoaded(allMovies));
        } catch (error) {
          debugPrint(error.toString());
        }
      }
      _isLoading = false;
    });
  }

  Future<List<Movie>> onMoviesFetched() async {
    List<Movie> allMovies = [];
    return allMovies;
  }

  Future<List<Movie>> onMoviesMoreFetched(int currentPage) async {
    List<Movie> allMovies = [];
    return allMovies;
  }
}
