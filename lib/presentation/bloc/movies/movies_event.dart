part of 'movies_bloc.dart';

@immutable
sealed class MoviesEvent extends Equatable {
  const MoviesEvent();

  @override
  List<Object> get props => [];
}

class FetchMovies extends MoviesEvent {
  const FetchMovies();

  @override
  List<Object> get props => [];
}

class FetchMoreMovies extends MoviesEvent {
  final int page;
  const FetchMoreMovies(this.page);

  @override
  List<Object> get props => [page];
}
