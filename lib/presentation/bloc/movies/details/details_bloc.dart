import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:peliculas/domain/entities/movie.dart';
import 'package:peliculas/infrastructure/datasources/moviedb_datasource.dart';
import 'package:peliculas/infrastructure/repositories/movie_repository_impl.dart';

part 'details_event.dart';
part 'details_state.dart';

class DetailsBloc extends Bloc<DetailsEvent, DetailsState> {
  DetailsBloc() : super(DetailsInitial()) {
    on<FetchDetails>(_onDetailsFetched);
  }
}

Future<void> _onDetailsFetched(
    FetchDetails event, Emitter<DetailsState> emit) async {
  final moviesRepo = MovieRepositoryImpl(MoviedbDatasource());
  emit(DetailsLoading());
  try {
    final movie = await moviesRepo.getMovieById(event.id);
    emit(DetailsLoaded(movie));
  } catch (error) {
    debugPrint(error.toString());
    emit(DetailsError(error.toString()));
  }
}
