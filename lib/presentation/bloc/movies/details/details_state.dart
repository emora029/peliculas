part of 'details_bloc.dart';

sealed class DetailsState extends Equatable {
  const DetailsState();

  @override
  List<Object> get props => [];
}

final class DetailsInitial extends DetailsState {}

class DetailsLoading extends DetailsState {}

class DetailsLoaded extends DetailsState {
  final Movie movie;
  const DetailsLoaded(this.movie);
}

class DetailsError extends DetailsState {
  final String error;
  const DetailsError(this.error);

  @override
  List<Object> get props => [error];
}
