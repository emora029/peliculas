part of 'details_bloc.dart';

sealed class DetailsEvent extends Equatable {
  const DetailsEvent();

  @override
  List<Object> get props => [];
}

class FetchDetails extends DetailsEvent {
  final String id;
  const FetchDetails(this.id);

  @override
  List<Object> get props => [];
}
