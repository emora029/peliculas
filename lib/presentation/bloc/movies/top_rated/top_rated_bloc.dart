import 'package:peliculas/domain/entities/movie.dart';
import 'package:peliculas/infrastructure/datasources/moviedb_datasource.dart';
import 'package:peliculas/infrastructure/repositories/movie_repository_impl.dart';
import 'package:peliculas/presentation/bloc/movies/movies_bloc.dart';

class TopRatedBloc extends MoviesBloc {
  final _moviesRepo = MovieRepositoryImpl(MoviedbDatasource());
  @override
  Future<List<Movie>> onMoviesFetched() async {
    return await _moviesRepo.getTopRated();
  }

  @override
  Future<List<Movie>> onMoviesMoreFetched(int currentPage) async {
    return await _moviesRepo.getTopRated(page: currentPage);
  }
}
