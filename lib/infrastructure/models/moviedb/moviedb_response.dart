import 'package:peliculas/infrastructure/models/moviedb/result_moviedb.dart';

class MovieDbResponse {
  final List<ResultMovieDB> results;

  MovieDbResponse({
    required this.results,
  });

  factory MovieDbResponse.fromJson(Map<String, dynamic> json) =>
      MovieDbResponse(
        results: List<ResultMovieDB>.from(
            json["results"].map((x) => ResultMovieDB.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
      };
}
