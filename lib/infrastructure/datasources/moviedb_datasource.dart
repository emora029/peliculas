import 'package:dio/dio.dart';
import 'package:peliculas/config/constants/environment.dart';
import 'package:peliculas/domain/datasources/movies_datasource.dart';
import 'package:peliculas/domain/entities/movie.dart';
import 'package:peliculas/infrastructure/mappers/movie_mapper.dart';
import 'package:peliculas/infrastructure/models/moviedb/movie_details.dart';
import 'package:peliculas/infrastructure/models/moviedb/moviedb_response.dart';

class MoviedbDatasource extends MoviesDataSource {
  final dio = Dio(BaseOptions(
    baseUrl: 'https://api.themoviedb.org/3',
    queryParameters: {
      'api_key': Environment.movieDBKey,
      'language': 'it',
    },
  ));

  List<Movie> _jsonToMovies(Map<String, dynamic> json) {
    final movieDBResponse = MovieDbResponse.fromJson(json);
    final List<Movie> movies = movieDBResponse.results
        .map((moviedb) => MovieMapper.movieDBToEntity(moviedb))
        .toList();
    return movies;
  }

  @override
  Future<List<Movie>> getNowPlaying({int page = 1}) async {
    try {
      final response = await dio.get(
        '/movie/now_playing',
        queryParameters: {'page': page},
      );
      return _jsonToMovies(response.data);
    } on DioException catch (e) {
      if (e.response != null) {
        throw Exception(e.response);
      } else {
        throw Exception(e.response);
      }
    }
  }

  @override
  Future<List<Movie>> getPopular({int page = 1}) async {
    try {
      final response = await dio.get(
        '/movie/popular',
        queryParameters: {'page': page},
      );

      return _jsonToMovies(response.data);
    } on DioException catch (e) {
      if (e.response != null) {
        throw Exception(e.response);
      } else {
        throw Exception(e.response);
      }
    }
  }

  @override
  Future<List<Movie>> getTopRated({int page = 1}) async {
    try {
      final response = await dio.get(
        '/movie/top_rated',
        queryParameters: {'page': page},
      );
      return _jsonToMovies(response.data);
    } on DioException catch (e) {
      if (e.response != null) {
        throw Exception(e.response);
      } else {
        throw Exception(e.response);
      }
    }
  }

  @override
  Future<List<Movie>> getUpcoming({int page = 1}) async {
    try {
      final response = await dio.get(
        '/movie/upcoming',
        queryParameters: {'page': page},
      );
      return _jsonToMovies(response.data);
    } on DioException catch (e) {
      if (e.response != null) {
        throw Exception(e.response);
      } else {
        throw Exception(e.response);
      }
    }
  }

  @override
  Future<Movie> getMovieById(String id) async {
    try {
      final response = await dio.get('/movie/$id');
      final movieDetails = MovieDetails.fromJson(response.data);
      final Movie movie = MovieMapper.movieDetailsToEntity(movieDetails);
      return movie;
    } on DioException catch (e) {
      if (e.response != null) {
        throw Exception(e.response);
      } else {
        throw Exception(e.response);
      }
    }
  }
}
